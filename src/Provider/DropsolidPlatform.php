<?php

namespace Dropsolid\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use Dropsolid\OAuth2\Client\Provider\Exception\DropsolidPlatformIdentityProviderException;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * Class DropsolidPlatform
 *
 * @package Dropsolid\OAuth2\Client\Provider
 */
class DropsolidPlatform extends GenericProvider
{

    /**
     * @inheritdoc
     */
    public function getDefaultScopes()
    {
        return ['cdp_admin'];
    }

    /**
     * @inheritdoc
     */
    protected function getAuthorizationHeaders($token = null)
    {
        if (!$token instanceof AccessToken) {
            return [];
        }

        return ['Authorization' => 'Bearer ' . $token->getToken()];
    }

    /**
     * @inheritdoc
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if ($response->getStatusCode() !== 200) {
            throw DropsolidPlatformIdentityProviderException::fromResponse($response);
        }
    }

    /**
     * @inheritdoc
     *
     * @return DropsolidPlatformResourceOwner
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        if (!isset($response['data'])) {
            throw new RuntimeException('Unexpected response from the resource owner call');
        }

        return new DropsolidPlatformResourceOwner($response['data']);
    }
}
