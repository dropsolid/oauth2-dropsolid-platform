<?php

namespace Dropsolid\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Tool\ArrayAccessorTrait;

/**
 * Class DropsolidPlatformResourceOwner
 *
 * @package Dropsolid\OAuth2\Client\Provider
 */
class DropsolidPlatformResourceOwner implements ResourceOwnerInterface
{
    use ArrayAccessorTrait {
        getValueByKey as arrayAccessorTraitGetValueByKey;
    }

    /**
     * @var array
     */
    protected $response;

    /**
     * DropsolidPlatformResourceOwner constructor.
     *
     * @param array $response
     */
    public function __construct(array $response = [])
    {
        $this->response = $response;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getValueByKey('id', '');
    }

    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return $this->response;
    }

    /**
     * Just the ArrayAccessorTrait's getValueByKey method, but with the first
     * parameter prefilled.
     *
     * @param $key
     * @param null $default
     * @return mixed
     */
    private function getValueByKey($key, $default = null)
    {
        return $this->arrayAccessorTraitGetValueByKey(
            $this->response,
            $key,
            $default
        );
    }
}
