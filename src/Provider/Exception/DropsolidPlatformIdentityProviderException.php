<?php

namespace Dropsolid\OAuth2\Client\Provider\Exception;

use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class DropsolidPlatformIdentityProviderException
 *
 * @package Dropsolid\OAuth2\Client\Provider\Exception
 */
class DropsolidPlatformIdentityProviderException extends IdentityProviderException
{
    /**
     * @param ResponseInterface $response
     * @param string $message
     * @return DropsolidPlatformIdentityProviderException
     */
    public static function fromResponse(
        ResponseInterface $response,
        $message = ''
    ) {
        return new static(
            $message,
            $response->getStatusCode(),
            $response->getBody()->getContents()
        );
    }
}
