<?php

namespace Dropsolid\OAuth2\Client\Http\Guzzle\Middleware;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Class RefreshTokenMiddleware
 *
 * @package \Dropsolid\OAuth2\Client\Http\Guzzle\Middleware
 */
class RefreshTokenMiddleware
{
    /**
     * @var AbstractProvider
     */
    private $provider;

    /**
     * @var AccessTokenInterface
     */
    private $token;

    /**
     * @var callable
     */
    private $refreshTokenCallback;

    /**
     * AuthenticationMiddleware constructor.
     *
     * @param AbstractProvider $provider
     * @param AccessTokenInterface $token
     * @param callable $refreshTokenCallback
     */
    public function __construct(
        AbstractProvider $provider,
        AccessTokenInterface $token,
        callable $refreshTokenCallback = null
    ) {
        $this->provider = $provider;
        $this->token = $token;
        $this->refreshTokenCallback = $refreshTokenCallback;
    }

    public function __invoke(callable $handler)
    {
        return function (RequestInterface $request, array $options) use ($handler) {
            if ($this->token->hasExpired()) {
                $this->refreshToken();
                $request = $request->withHeader('Authorization', 'Bearer '.$this->token->getToken());
            }

            return $handler($request, $options);
        };
    }

    /**
     * @throws IdentityProviderException
     */
    private function refreshToken()
    {
        $this->token = $this->provider->getAccessToken('client_credentials');
        if ($this->refreshTokenCallback) {
            call_user_func($this->refreshTokenCallback, $this->token);
        }
    }
}
