# Dropsolid Platform Provider for OAuth 2.0 Client

This package provides [Dropsolid Platform](https://platform.dropsolid.com/) OAuth 2.0
support for the PHP League's
[OAuth 2.0 Client](https://github.com/thephpleague/oauth2-client).

## Installation

```
composer require dropsolid/oauth2-dropsolid-platform
```

## Usage

See the [League documentation](http://oauth2-client.thephpleague.com/usage/). 
This package provides a Dropsolid Platform specific provider you can use instead of the 
generic one.

```php
$provider = new Dropsolid\OAuth2\Client\Provider\DropsolidPlatform([
    'clientId' => 'your-client-id',
    'clientSecret' => 'your-client-secret',
    'redirectUri' => 'https://example.com/your-redirect-url',
]);
```
